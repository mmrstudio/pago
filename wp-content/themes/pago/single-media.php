<?php

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        $media_type = get_field('media_type');

        if($media_type == 'video') :

            $video_type = get_field('video_type');
            $video_thumbnail = get_field('video_thumbnail');
            $video_url = get_field('video_url');
            $youtube_url = get_field('youtube_url');

            $video_data = [
                'type' => 'single',
                'videos' => [
                    [
                        'videoType' => $video_type,
                        'videoUrl' => $video_type == 'url' ? $video_url : $youtube_url,
                        'videoId' => $video_type == 'youtube' ? parse_youtube_id($youtube_url) : false,
                        'embedUrl' => $video_type == 'youtube' ? youtube_embed_url($youtube_url) : false,
                        'thumbnail' => acf_image($video_thumbnail, 'large', false),
                    ],
                ],
            ];

        endif;

        if($media_type == 'document') :

            $file_attachment = get_field('file_attachment');

            if($file_attachment) :
                $file_path = get_attached_file($file_attachment['ID']);
                $file_size = get_file_size($file_path);
                $file_type = strtoupper(explode('/', $file_attachment['mime_type'] )[1]);
            endif;

        endif;

        global $wp;
        $current_url = home_url(add_query_arg(array(), $wp->request));

        $share_links = [
            'facebook' => [
                'name' => 'Facebook',
                'url' => 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($current_url),
            ],

            'linkedin' => [
                'name' => 'LinkedIn',
                'url' => 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($current_url),
            ],
            'email' => [
                'name' => __('Email', 'pago'),
                'url' => 'mailto:?subject=' . wp_title('|', false) . '&body=' . $current_url,
            ]
        ];

        get_header();

?>

<div class="sections">

    <section class="section section--page-heading content">

        <div class="section__container">

            <div class="section__inner">
                <h1><?php the_title(); ?></h1>
            </div>

        </div>

    </section>

    <section class="section section--content-row content">

        <div class="section__container">

            <div class="section__inner">

                <div class="content__columns">

                    <div class="content__columns__column" data-width="half">

                        <div class="component component--text-block">

                            <?php the_content(); ?>

                            <?php if($media_type == 'document' && $file_attachment) : ?>
                            <div class="component__buttons button__group">
                                <a href="<?php echo $file_attachment['url']; ?>" class="button button--icon">
                                    <div class="button__inner">
                                        <div class="button__icon icon-download"></div>
                                        <span class="button__label"><?php echo __('Download', 'pago'); ?> (<?php echo $file_type . ' ' . $file_size; ?> )</span>
                                    </div>
                                </a>
                            </div>
                            <?php endif; ?>

                        </div>

                    </div>

                    <div class="content__columns__column" data-width="half">

                        <?php if($media_type == 'video') : ?>
                        <div class="sections--media__video">
                            <div class="component component--video-single" data-video-data='<?php echo json_encode($video_data); ?>'></div>
                        </div>
                        <?php else : ?>
                        <div class="sections--media__image">
                            <?php the_post_thumbnail('full'); ?>
                        </div>
                        <?php endif; ?>

                        <div class="component--share sections--media__share">
                            <span><?php echo __('Share this media', 'pago'); ?></span>
                            <ul class="social-icons">
                                <?php foreach($share_links as $network => $link) : ?>
                                <li><a href="<?php echo $link['url']; ?>" class="icon-<?php echo $network; ?>" <?php if($network != 'email') : ?>target="_blank"<?php endif; ?>><?php echo $link['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <nav class="component--sidebar__nav">
                            <div class="component--sidebar__nav__prev"><?php previous_post_link('%link', __('Previous', 'pago')); ?></div>
                            <div class="component--sidebar__nav__next"><?php next_post_link('%link', __('Next', 'pago')); ?></div>
                        </nav>


                    </div>

                </div>

            </div>

        </div>

    </section>

</div>

<?php

    endwhile; // end loop

    get_footer();

?>
