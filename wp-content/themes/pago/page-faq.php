<?php
    /* Template Name: Page - FAQs */

    // start loop
	if(have_posts()) while (have_posts()) : the_post();



        get_header();

				$panels = get_field('panels');

?>

<div class="sections"  >


    <section class="section section--page-heading section--v4 content">

        <div class="section__container">

            <div class="section__inner">
									<h1 class="heading1--small"><?php the_title(); ?></h1>
									<div class="content__form">
											<?php the_content();?>
									</div>

									<div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

											<div class="component component--accordion">

													<?php foreach($panels as $i => $panel) : ?>

													<div class="component--accordion__panel content__collapsible">
															<div class="component--accordion__panel__toggle content__collapsible__toggle"><?php echo $panel['panel_title']; ?></div>
															<div class="component--accordion__panel__content content__collapsible__content">
																	<div class="component--accordion__panel__content__inner">
																			<?php echo $panel['panel_content']; ?>
																	</div>
															</div>
													</div>

													<?php endforeach; ?>

											</div>

									</div>
            </div>

        </div>

    </section>



</div>

<?php

    endwhile; // end loop

    get_footer();

?>
