<?php

    function fb_curl_post($url, $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }

    function fb_curl_get($url, $params) {
        $ch = curl_init();
        $url = $url . '?' . http_build_query($params, null, '&');
        //echo $url; exit;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }

    function fb_get_post_count($post_id, $meta) {

        $post_count = 0;

        $fb_query = [
            'access_token' => get_field('facebook_access_token', 'option'),
            'summary' => 'true',
        ];

        $fb_response = fb_curl_get('https://graph.facebook.com/v2.8/' . $post_id . '/' . $meta, $fb_query);

        if($fb_response) :
            $fb_response = json_decode($fb_response);
            $post_count = $fb_response->summary->total_count;
        endif;

        return $post_count;

    }

    function get_fb_page_token($app_id, $secret) {
        $url = 'https://graph.facebook.com/oauth/access_token';
        $token_params = array(
            "type" => "client_cred",
            "client_id" => $app_id,
            "client_secret" => $secret
        );
        return str_replace('access_token=', '', fb_curl_post($url, $token_params));
    }

	// get the latest tweets
	function get_latest_facebook_posts($limit=10, $delete_transient=false) {

        $fb_config = array(
            'page_id' => get_field('facebook_page_id', 'option'),
            'page_name' => get_field('facebook_page_name', 'option'),
    		'app_id' => get_field('facebook_app_id', 'option'),
    		'app_secret' => get_field('facebook_app_secret', 'option'),
    		'access_token' => get_field('facebook_access_token', 'option'),
    	);

        //print_r($fb_config); exit;

		// delete transient if you want to test
		if($delete_transient) delete_transient( 'latest_fb_posts' );

		if ( false === ( $latest_posts = get_transient( 'latest_fb_posts' ) ) ) :

			// setup empty array
			$latest_posts = array();

            // grab the response
            $fb_query = [
                'access_token' => $fb_config['access_token'],
                'debug' => 'all',
                'fields' => 'message,full_picture,created_time,link,actions,attachments',
                'format' => 'json',
                'method' => 'get',
                'pretty' => '0',
                'suppress_http_code' => '1',
                'limit' => $limit,
            ];

			$fb_response = fb_curl_get('https://graph.facebook.com/v2.8/' . $fb_config['page_id'] . '/feed', $fb_query);

            //print_r($fb_response); exit;

			if( $fb_response ) :

                $fb_response = json_decode($fb_response);

                //print_r($fb_response); exit;

                // get page image
                $fb_page_image_response = fb_curl_get('https://graph.facebook.com/v2.8/' . $fb_config['page_id'] . '/picture', ['redirect' => '0']);
                $fb_page_image_response = json_decode($fb_page_image_response);

                //print_r($fb_page_image_response); exit;

				foreach( $fb_response->data as $post_obj ) :

					$fb_post = array(
                        'platform' => 'facebook',
						'id' => $post_obj->id,
                        'link' => $post_obj->link,
                        'type' => 'post',
                        'timestamp' => $post_obj->created_time,
						'date' => date(get_option('date_format'), strtotime($post_obj->created_time) ),
                        'date_hr' => time2str($post_obj->created_time),
						'text' => twitterify($post_obj->message),
                        'image' => isset($post_obj->attachments->data[0]) ? $post_obj->attachments->data[0]->media->image->src : '',
                        'image_aspect' => isset($post_obj->attachments->data[0]) ? ($post_obj->attachments->data[0]->media->image->height / $post_obj->attachments->data[0]->media->image->width) * 100 : 0,
                        'user_name' => $fb_config['page_name'],
                        'user_image' => $fb_page_image_response->data->url,
                        'likes' => fb_get_post_count($post_obj->id, 'likes'),
                        'comments' => fb_get_post_count($post_obj->id, 'comments'),
					);

                    $latest_posts[] = $fb_post;

				endforeach;

			endif;

            $latest_posts = json_encode( $latest_posts );

			set_transient( 'latest_fb_posts' , $latest_posts , 2 * HOUR_IN_SECONDS );

			//print_r($latest_posts); exit;

		endif;

        $latest_posts = json_decode( $latest_posts );

        //print_r($latest_posts); exit;

		return $latest_posts;

	}

    //get_latest_facebook_posts(); exit;

    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array (
        	'key' => 'group_facebook_settings',
        	'title' => 'Facebook API',
        	'fields' => array (
                array (
                    'key' => 'field_facebook_page_id',
                    'label' => 'Page ID',
                    'name' => 'facebook_page_id',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'field_facebook_page_name',
                    'label' => 'Page Name',
                    'name' => 'facebook_page_name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
        		array (
        			'key' => 'field_facebook_app_id',
        			'label' => 'App ID',
        			'name' => 'facebook_app_id',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        		array (
        			'key' => 'field_facebook_app_secret',
        			'label' => 'App Secret',
        			'name' => 'facebook_app_secret',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        		array (
        			'key' => 'field_facebook_access_token',
        			'label' => 'Access Token',
        			'name' => 'facebook_access_token',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        	),
        	'location' => array (
        		array (
        			array (
        				'param' => 'options_page',
        				'operator' => '==',
        				'value' => 'options',
        			),
        		),
        	),
        	'menu_order' => 1,
        	'position' => 'normal',
        	'style' => 'default',
        	'label_placement' => 'top',
        	'instruction_placement' => 'label',
        	'hide_on_screen' => '',
        	'active' => 1,
        	'description' => '',
        ));

    endif;

    function facebook_access_token_field($field) {

        if($field['key'] == 'field_facebook_access_token') :
            echo '<a href="' . site_url('wp-admin/admin.php?page=options&get_fb_access_token') . '" accesskey="p" class="button button-primary button-large" style="margin-top: 10px;">Get Access Token</a>';
        endif;

    }

    add_action( 'acf/render_field/type=text', 'facebook_access_token_field', 10, 1 );

    if( isset($_GET['get_fb_access_token']) ) :

        $fb_config = array(
    		'app_id' => get_field('facebook_app_id', 'option'),
    		'app_secret' => get_field('facebook_app_secret', 'option'),
    	);

        //print_r($fb_config); exit;

        $access_token = get_fb_page_token($fb_config['app_id'], $fb_config['app_secret']);

        //echo '$access_token - ' . $access_token; exit;

        update_field('field_facebook_access_token', $access_token, 'option');

        wp_redirect(site_url('wp-admin/admin.php?page=options'));
        exit;

    endif;
