<?php

    function add_ajax_action($action, $callback) {
        add_action('wp_ajax_'.$action, $callback);
        add_action('wp_ajax_nopriv_'.$action, $callback);
    }

    function request_param($key, $default) {
        return (isset($_REQUEST[$key])) ? trim($_REQUEST[$key]) : $default ;
    }

    function json_response($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }
