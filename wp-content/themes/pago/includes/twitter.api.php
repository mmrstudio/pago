<?php

	// grab the twitter oath class
    require_once (TEMPLATEPATH . '/includes/twitter/twitteroauth.php');

	// twitter - style links, hash tags etc.
	// function twitterify($ret) {
	//   $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
	//   $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
	//   $ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
	//   $ret = preg_replace("/#(\w+)/", "<a href=\"http://twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
	// return $ret;
	// }

	// twitter - remove emoji
	// function removeEmoji($text) {
	//     $clean_text = "";
	//     // Match Emoticons
	//     $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
	//     $clean_text = preg_replace($regexEmoticons, '', $text);
	//     // Match Miscellaneous Symbols and Pictographs
	//     $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
	//     $clean_text = preg_replace($regexSymbols, '', $clean_text);
	//     // Match Transport And Map Symbols
	//     $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
	//     $clean_text = preg_replace($regexTransport, '', $clean_text);
	//     // Match Miscellaneous Symbols
	//     $regexMisc = '/[\x{2600}-\x{26FF}]/u';
	//     $clean_text = preg_replace($regexMisc, '', $clean_text);
	//     // Match Dingbats
	//     $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
	//     $clean_text = preg_replace($regexDingbats, '', $clean_text);
	//     return $clean_text;
	// }

	// get the latest tweets
	function get_latest_tweets($limit=10, $delete_transient=false) {

        $twitter_config = array(
    		'consumer_key' => get_field('twitter_consumer_key', 'option'),
    		'consumer_secret' => get_field('twitter_consumer_secret', 'option'),
    		'oauth_token' => get_field('twitter_oauth_token', 'option'),
    		'oauth_token_secret' => get_field('twitter_oauth_token_secret', 'option'),
    	);

        //print_r($twitter_config); exit;

        if(!$twitter_config['consumer_key'] || !$twitter_config['consumer_secret'] || !$twitter_config['oauth_token'] || !$twitter_config['oauth_token_secret']) return [];

		$twitter = new TwitterOAuth($twitter_config['consumer_key'], $twitter_config['consumer_secret'], $twitter_config['oauth_token'], $twitter_config['oauth_token_secret']);

		// delete transient if you want to test
		if($delete_transient) delete_transient( 'latest_tweets' );

		if ( false === ( $latest_tweets = get_transient( 'latest_tweets' ) ) ) :

			// setup empty array
			$latest_tweets = array();

			// grab the response
			$twitter_response = $twitter->get('statuses/user_timeline' , array( 'count' => $limit ));

            //print_r($twitter_response); exit;

			// if we have a response, add each to our array of latest tweets
			if( $twitter_response ) :

                //$twitter_response = json_decode(json_encode($twitter_response)); // for some reason this makes emoji work properly

				foreach( $twitter_response as $tweet_obj ) :

					$tweet = array(
                        'platform' => 'twitter',
						'id' => $tweet_obj->id,
                        'link' => 'https://twitter.com/_/status/' . $tweet_obj->id,
                        'type' => 'text',
                        'timestamp' => $tweet_obj->created_at,
						'date' => date(get_option('date_format'), strtotime($tweet_obj->created_at) ),
                        'date_hr' => time2str($tweet_obj->created_at),
						'text' => twitterify( $tweet_obj->text ),
                        'image' => false,
                        'image_aspect' => 0,
                        'user_name' => $tweet_obj->user->name,
                        'user_image' => $tweet_obj->user->profile_image_url,
                        'retweets' => $tweet_obj->retweet_count,
                        'favorites' => $tweet_obj->favorite_count,
					);

                    // has media?
                    if(count($tweet_obj->entities->media) > 0) :
                        $tweet['type'] = 'image';
                        $tweet['image'] = $tweet_obj->entities->media[0]->media_url;
                    endif;

                    $latest_tweets[] = $tweet;

				endforeach;

			endif;

            $latest_tweets = json_encode( $latest_tweets );

			set_transient( 'latest_tweets' , $latest_tweets , 2 * HOUR_IN_SECONDS );

			//print_r($latest_tweets); exit;

		endif;

        $latest_tweets = json_decode( $latest_tweets );

        //print_r($latest_tweets); exit;

		return $latest_tweets;

	}

    //get_latest_tweets(); exit;

    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array (
        	'key' => 'group_twitter_settings',
        	'title' => 'Twitter API',
        	'fields' => array (
        		array (
        			'key' => 'field_twitter_consumer_key',
        			'label' => 'Client ID',
        			'name' => 'twitter_consumer_key',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        		array (
        			'key' => 'field_twitter_consumer_secret',
        			'label' => 'Client Secret',
        			'name' => 'twitter_consumer_secret',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        		array (
        			'key' => 'field_twitter_oauth_token',
        			'label' => 'OAuth Token',
        			'name' => 'twitter_oauth_token',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
                array (
        			'key' => 'field_twitter_oauth_token_secret',
        			'label' => 'OAuth Token Secret',
        			'name' => 'twitter_oauth_token_secret',
        			'type' => 'text',
        			'instructions' => '',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'default_value' => '',
        			'placeholder' => '',
        			'prepend' => '',
        			'append' => '',
        			'maxlength' => '',
        		),
        	),
        	'location' => array (
        		array (
        			array (
        				'param' => 'options_page',
        				'operator' => '==',
        				'value' => 'options',
        			),
        		),
        	),
        	'menu_order' => 1,
        	'position' => 'normal',
        	'style' => 'default',
        	'label_placement' => 'top',
        	'instruction_placement' => 'label',
        	'hide_on_screen' => '',
        	'active' => 1,
        	'description' => '',
        ));

    endif;
