<?php

    /* Template Name: Page - Country */
    if(have_posts()) while (have_posts()) : the_post();

    get_header();

?>

    <?php
        //Set variables for the template part
        $header_mobile_image = get_field('header_mobile_image');
        $header_desktop_image = get_field('header_desktop_image');

        set_query_var('post_title', get_the_title());
        set_query_var('post_parent', get_the_ID());
        set_query_var('post_active', null);
        set_query_var('post_header_mobile_image', $header_mobile_image['url']);
        set_query_var('post_header_desktop_image', $header_desktop_image['url']);

        get_template_part('parts/hero', 'country');
    ?>

<?php

    endwhile; // end loop

    get_footer();

?>