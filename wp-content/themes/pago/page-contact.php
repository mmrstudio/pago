<?php
    /* Template Name: Page - Contact */

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        $form = get_field('form');


        //print_r($form); exit;

        get_header();

?>

<div class="sections"   data-namespace="formpage">

    <section class="section section--page-heading section--v4 content">

        <div class="section__container">

            <div class="section__inner">
                <h1 class="heading1--small"><?php the_title(); ?></h1>
								<div class="content__form">
                    <?php the_content();?>
                </div>
            </div>

        </div>

    </section>



		<section class="section section--content-row section--v3 content">

        <div class="section__container">

            <div class="section__inner">
								<h3 class="heading3--small"><?php echo __('Our offices', 'pago'); ?></h3>
                <div class="office">

									<?php

										// check if the repeater field has rows of data
										if( have_rows('our_offices') ):
                      $i= 0;
										// loop through the rows of data
											while ( have_rows('our_offices') ) : the_row(); $i++; ?>

													<div class="office__single">
															<div class="office__image index<?php echo $i%3; ?>">
																	<img src="<?php echo get_sub_field('office_picture'); ?>">
															</div>
															<div class="office__title">
																	<h4><?php the_sub_field('office_title'); ?></h4>
															</div>
															<div class="office__add">
																	<?php the_sub_field('office_address');; ?>
															</div>
															<div class="office__maplink">
																	<a href="<?php the_sub_field('map_link'); ?>"><?php echo __('View map', 'pago'); ?></a>
															</div>
												</div>
										<?php	endwhile;

										else :

											// no rows found

										endif;

										?>
                </div>

            </div>

        </div>

    </section>
		<section class="section section--page-heading section--v5 content">

        <div class="section__container">

            <div class="section__inner">

								<div class="work">
                    <?php the_field('working_with_us'); ?>
                </div>
            </div>

        </div>

    </section>
</div>

<?php

    endwhile; // end loop

    get_footer();

?>
