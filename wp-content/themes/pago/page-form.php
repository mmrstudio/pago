<?php

    /* Template Name: Page - Form */

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        $form = get_field('form');
        $page_title = isset($_GET['p']) ? $_GET['p'] : $form['title'];

        //print_r($form); exit;

        get_header();

?>

<div class="sections"   data-namespace="formpage">


    <section class="section section--content-row section--form content">

        <div class="section__container">

            <div class="section__inner">
								<h1  class="heading1--red"><?php echo $page_title; ?></h1>

								<div class="content__columns">

										<div class="content__columns__column" data-width="full">

												<div class="component component--text-block">
														<p><?php echo __('Please complete the form below to apply:', 'pago'); ?></p>
												</div>

										</div>

								</div>

                <div class="content__form">
                    <?php echo do_shortcode('[gravityform id="'. $form['id'] .'" title="false" description="false"]'); ?>
                </div>

            </div>

        </div>

    </section>

</div>

<?php

    endwhile; // end loop

    get_footer();

?>
