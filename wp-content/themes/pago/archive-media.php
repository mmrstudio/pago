<?php

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        $request = _get('r', false);

        $is_ajax = isAjax();
        //$is_ajax = true;

        if($request === 'json') :

            $media_query = [
                'post_type' => 'media',
                'posts_per_page' => -1,
                'tax_query' => [],
            ];

            $terms = [];
            // $get_terms = _get('terms', '');
            // $get_terms = explode(';', $get_terms);
            //
            // foreach($get_terms as &$term) :
            //     $term = explode(':', $term);
            //     $terms[$term[0]] = explode(',', $term[1]);
            // endforeach;

            $terms = [
                'territories' => _getParram('tr'),
                'solutions' => _getParram('sl'),
                'category' => _getParram('ct'),
                'medium' => _getParram('md'),
            ];

            //print_r($terms); exit;

            // get parrams
            $territories_parram = isset($terms['territories']) ? $terms['territories'] : false;
            $solutions_parram = isset($terms['solutions']) ? $terms['solutions'] : false;
            $category_parram = isset($terms['category']) ? $terms['category'] : false;
            $medium_parram = isset($terms['medium']) ? $terms['medium'] : false;

            function add_tax_query($tax, $parrams, &$media_query) {

                if($parrams) :
                    $media_query['tax_query'][] = [
                        'taxonomy' => $tax,
                        'field' => 'slug',
                        'terms' => $parrams ? $parrams : get_terms_array($tax),
                    ];
                endif;

            }

            // append queries
            add_tax_query('media_territory', $territories_parram, $media_query);
            add_tax_query('media_solutions', $solutions_parram, $media_query);
            add_tax_query('media_category', $category_parram, $media_query);
            add_tax_query('media_medium', $medium_parram, $media_query);

            if(count($tax_query) > 0) :
                $tax_query[] = ['relation' => 'AND'];
            endif;

            // SORT
            $get_sort = _get('sort', ['date:desc']);
            if($get_sort) :
                $sort = explode(':', $get_sort);
                $media_query['orderby'] = $sort[0];
                $media_query['order'] = strtoupper($sort[1]);
            endif;

            // SEARCH
            $get_search = _get('search', false);
            if($get_search) :
                $media_query['s'] = $get_search;
            endif;

            //print_r($media_query); exit;

            $get_media_posts = new WP_Query($media_query);

            //print_r($get_media_posts); exit;

            $media_posts = [];

            if($get_media_posts->have_posts()) :
                while($get_media_posts->have_posts()) : $get_media_posts->the_post();

                    $post_id = get_the_ID();

                    $media_posts[] = [
                        'id' => $post_id,
                        'date' => get_the_date(),
                        'title' => html_entity_decode(get_the_title()),
                        'thumbnail' => get_the_post_thumbnail_url($post_id, 'large'),
                        'type' => get_field('media_type'),
                        'introduction' => strip_tags(get_the_content()),
                        'permalink' => get_permalink(),
                    ];

                endwhile;
                wp_reset_query();
            endif;

            $media_posts[] = $media_query;

            //print_r($media_posts); exit;

            json_response($media_posts);

        else :

            $media_page = get_page_by_path('media');

            $terms = [
                'title' => $media_page->post_title,
                'url' => site_url('/media/'),
                'noImage' => THEME_URL . '/images/no-image.png',
                'terms' => [
                    'territories' => [
                        'title' => 'Territories',
                        'parram' => 'tr',
                        'terms' => get_terms_array('media_territory', [], true)
                    ],
                    'solutions' => [
                        'title' => 'Solutions',
                        'parram' => 'sl',
                        'terms' => get_terms_array('media_solutions', [], true)
                    ],
                    'categories' => [
                        'title' => 'Categories',
                        'parram' => 'ct',
                        'terms' => get_terms_array('media_category', [], true)
                    ],
                    'mediums' => [
                        'title' => 'Medium',
                        'parram' => 'md',
                        'terms' => get_terms_array('media_medium', [], true)
                    ],
                ],
                'sortOptions' => [
                    'date:desc' => 'Date (Newest - Oldest)',
                    'date:asc' => 'Date (Oldest - Newest)',
                    'title:asc' => 'Title (Ascending)',
                    'title:desc' => 'Title (Descending)',
                ],
            ];

            //print_r($terms); exit;

            get_header();

?>

<div class="sections sections--media" data-config='<?php echo json_encode($terms); ?>'></div>

<?php

            get_footer();

        endif;

    endwhile; // end loop

?>
