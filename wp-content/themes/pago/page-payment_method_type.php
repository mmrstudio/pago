<?php

    /* Template Name: Page - Payment Method Type*/
    if(have_posts()) while (have_posts()) : the_post();

    get_header();

?>

    <?php

        $post_parent = wp_get_post_parent_id(get_the_ID());
        $header_mobile_image = get_field('header_mobile_image', $post_parent);
        $header_desktop_image = get_field('header_desktop_image', $post_parent)

        //Set variables for the template part
        set_query_var('post_title', get_the_title($post_parent));
        set_query_var('post_parent', $post_parent);
        set_query_var('post_active', get_the_ID());
        set_query_var('post_header_mobile_image', $header_mobile_image['url']);
        set_query_var('post_header_desktop_image', $header_desktop_image['url']);

        get_template_part('parts/hero', 'country');
    ?>

    <section  class="section">
    <div  class="section__container">
        <div class="section__title section__title--center">
            <h2 class="no-margin"><?php the_title(); ?></h2>
        </div>
    </div>
</section>

<div class="sections" id="infogcomp1">
        
    <div class="infog-wrapper-outer">
        <div class="infog-wrapper-inner">
            <div  class="section infog-section">
                <div class="section__container infog-section-container">
                    <div class="infog">
                    

                        <?php
                            //Set variables for the template part
                            set_query_var('post_parent', get_the_ID());

                            get_template_part('parts/infog', 'menu');
                        ?>
                        
                        <div class="infog-tiles">
                            <?php //get_template_part( 'parts/infog', 'tiles' ); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        /*while ( have_rows('below_tiles') ) : the_row();
            if( get_row_layout() == 'below_tiles_list' ):
               get_template_part( 'parts/below_tiles', 'list' );
            elseif( get_row_layout() == 'below_tiles_video' ):
                get_template_part( 'parts/below_tiles', 'video' );
            endif;
        endwhile;*/
    ?>
</div>

<?php

    endwhile; // end loop

    get_footer();

?>
