

            <footer class="footer">

                <div class="footer__container">

                    <div class="footer__text">

                        <div class="footer__copyright">
                            <span class="text"><?php echo write_footer_text(get_field('copyright_text', 'option')); ?></span>
                            <span> | </span>
                            <a href="<?php echo get_field('privacy_link', 'option'); ?>"><?php echo __('Privacy Policy', 'pago'); ?></a>
                        </div>
                    </div>

                    <ul class="footer__social">
                        <?php if($social_facebook = get_field('social_facebook', 'option')) : ?><li><a href="<?php echo $social_facebook; ?>" class="icon-facebook footer__social-link" target="_blank" title="Pago on Facebook"></a></li><?php endif; ?>
                        <?php if($social_twitter = get_field('social_twitter', 'option')) : ?><li><a href="<?php echo $social_twitter; ?>" class="icon-twitter footer__social-link" target="_blank" title="Pago on Twitter"></a></li><?php endif; ?>
                        <?php if($social_linkedin = get_field('social_linkedin', 'option')) : ?><li><a href="<?php echo $social_linkedin; ?>" class="icon-linkedin footer__social-link" target="_blank" title="Pago on LinkedIn"></a></li><?php endif; ?>
                        <?php if($social_youtube = get_field('social_youtube', 'option')) : ?><li><a href="<?php echo $social_youtube; ?>" class="icon-youtube footer__social-link" target="_blank" title="Pago on YouTube"></a></li><?php endif; ?>
                        <?php if($social_instagram = get_field('social_instagram', 'option')) : ?><li><a href="<?php echo $social_instagram; ?>" class="icon-instagram footer__social-link" target="_blank" title="Pago on Instagram"></a></li><?php endif; ?>
                    </ul>

                </div>

            </footer>
            <?php //</div> ?>


            <?php get_template_part('parts/menu', 'overlay'); ?>

            <?php wp_footer(); ?>

    </body>

</html>
