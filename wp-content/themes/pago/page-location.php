<?php

    /* Template Name: Page - Location */

    get_header();

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        $category = 'premier-events';

?>


    <div class="content">

        <section class="content__section content__section--no-padding">

            <div class="content__section__inner">

                <div class="content__category-header content__category-header--<?php echo $category; ?>">

                    <div class="content__category-header__title">

                        <div class="content__category-header__logo logo <?php echo $category; ?>-logo">
                            <h1 class="content__category-header__logo__inner logo__inner <?php echo $category; ?>-logo__inner"><?php echo mop_category_title($category); ?></h1>
                        </div>

                        <h2 class="content__category-header__sub-title"><?php echo __('Location', 'pago'); ?></h2>

                    </div>

                    <div class="content__category-header__wunderlust"></div>
                    <div class="content__category-header__gradient"></div>

                </div>

            </div>

        </section>

        <section class="content__section">

            <div class="content__section__inner">

                <div class="content__text-block">
                    <?php the_content(); ?>
                </div>

            </div>

        </section>

        <section class="content__section">

            <div class="content__section__inner">

                <div class="directions" id="directions"></div>

            </div>

        </section>

        <section class="content__section">

            <div class="content__section__inner">

                dsfhjksdfhjkds fhjksd fhjdkshjk

            </div>

        </section>

    </div>

<?php

    endwhile; // end loop

    get_footer();

?>
