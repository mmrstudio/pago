
    <?php get_template_part('parts/common', 'header'); ?>

    <?php //get_template_part('parts/nav', 'responsive'); ?>

    <?php $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' ); ?>

    <header class="header">

        <div class="header__map-menu">
            <?php //echo file_get_contents( get_stylesheet_directory_uri() . '/dist/map-menu.svg' ); ?>
        </div>

        <div class="header__map-text">
          <h1 class="header__map-text-h1"><?php the_field('map_menu_title', 'option');?></h1>

          <p class="header__map-text-p"><?php the_field('map_menu_text', 'option');?></p>
          <a href="<?php the_field('map_menu_vertical_link_page', 'option');?>" class="header__map-text-transform"><?php the_field('map_menu_vertical_link_text', 'option');?></a>


          <?php

          // check if the repeater field has rows of data
          if( have_rows('map_menu_text_links', 'option') ):?>
            <ul class="header__map-text-links">
             <?php while ( have_rows('map_menu_text_links', 'option') ) : the_row();?>
                  <li class="header__map-text-li"><a href="<?php the_sub_field('page');?>" class="header__map-text-link"><?php the_sub_field('link_text');?></a></li>
              <?php endwhile;?>
            </ul>
          <?php endif;?>

        </div>

        <div class="header__bar-menu">

          <div class="tiles-menu-wrap">
           <ul class="tiles-menu tiles-menu--up">

               <li class="tiles-menu__tile">
                  <a href="#" class="tiles-menu__link tiles-menu__link--map">
                    <span class="tiles-menu__link-sp tiles-menu__link-sp--icon"></span>
                    <span class="tiles-menu__link-sp"><?php echo __('Change Country', 'pago'); ?></span>
                  </a>
                </li>

                <?php if(count($languages) > 0) : ?>
                <li class="tiles-menu__tile">
                  <a href="#" class="tiles-menu__link tiles-menu__link--globe">
                    <span class="tiles-menu__link-sp tiles-menu__link-sp--icon"></span>
                    <span class="tiles-menu__link-sp"><?php echo __('Change Language', 'pago'); ?></span>
                  </a>
                   <ul class="tiles-menu__sub-menu">
                       <?php foreach($languages as $language) : ?>
                       <li><a href="<?php echo $language['url']; ?>" class="tiles-menu__sub-menu-link <?php echo $language['active'] ? 'tiles-menu__sub-menu-link--active' : ''; ?>"><?php echo $language['native_name']; ?></a></li>
                       <?php endforeach; ?>
                   </ul>
                </li>
                <?php endif; ?>

                <li class="tiles-menu__tile">
                    <a href="<?php the_field('map_menu_get_started_page', 'option');?>" class="tiles-menu__link tiles-menu__link--speech">
                      <span class="tiles-menu__link-sp tiles-menu__link-sp--icon"></span>
                      <span class="tiles-menu__link-sp"><?php echo __('Get Started', 'pago'); ?></span>
                    </a>
                </li>

           </ul>
          </div>

          <ul class="logo-toggle">
            <li class="logo-toggle__logo">
              <a href="<?php echo get_site_url(); ?>" class="logo-toggle__logo-link"></a>
            </li>

            <li class="logo-toggle__toggle">
              <a href="#" class="logo-toggle__toggle-link"></a>
            </li>
          </ul>

        </div>

    </header>
