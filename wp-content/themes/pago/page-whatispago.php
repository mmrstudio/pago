<?php
    /* Template Name: Page - What is Pago */

    // start loop
	if(have_posts()) while (have_posts()) : the_post();



        get_header();



?>
<div class="sections" >
	<section class="section section--content-row content">

        <div class="section__container">

            <div class="section__inner">
								<h1 class="heading1--small"><?php the_title(); ?></h1>

                <div class="content__columns">
					<div class="section__title">
							<h2 class="heading2--red"><?php the_content(); ?></h2>
					</div>


					<?php

					// check if the repeater field has rows of data
					if( have_rows('half_width_column_content') ):

					 	// loop through the rows of data
					    while ( have_rows('half_width_column_content') ) : the_row(); ?>


									<div class="content__columns__column <?php if(get_sub_field('grey_background_on_text')): echo "greybg"; endif; ?>" data-width="half"  >

										 <div class="component component--text-block">


																<?php if(get_sub_field('image')): ?>
																			<?php if(get_sub_field('image_position')=='top'): ?>
																							<?php if(get_sub_field('image_red_border')): ?>
																							<div class="feature_image">
																							<?php endif;?>
																									<img src="<?php echo get_sub_field('image'); ?>">
																							<?php if(get_sub_field('image_red_border')): ?>
																							</div>
																							<?php endif;?>
																					<?php the_sub_field('text'); ?>

																			<?php elseif(get_sub_field('image_position')=='bottom'): ?>

																				<?php the_sub_field('text'); ?>
																				<?php if(get_sub_field('image_red_border')): ?>
																				<div class="feature_image">
																				<?php endif;?>
																						<img src="<?php echo get_sub_field('image'); ?>">
																				<?php if(get_sub_field('image_red_border')): ?>
																				</div>
																				<?php endif;?>

																			<?php endif;?>
																<?php else: ?>
																			<?php the_sub_field('text'); ?>
																<?php endif;?>

										 </div>
								 </div>

					   <?php endwhile;

					else :

					    // no rows found

					endif;

					?>




        </div>

    </section>
</div>
<?php

    endwhile; // end loop

    get_footer();

?>
