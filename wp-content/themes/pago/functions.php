<?php

    date_default_timezone_set('Australia/Melbourne');

    // Composer autoload
    //require_once ABSPATH . '../vendor/autoload.php';

    // Load extra helper functions
    require_once (TEMPLATEPATH . '/includes/helper_functions.php');
    require_once (TEMPLATEPATH . '/includes/ajax_functions.php');

    // Set default CONSTANTS
    define( 'SITE_NAME' , get_bloginfo('name') );
    define( 'THEME_URL' , str_replace('http:', '', get_template_directory_uri()) );
    define( 'THEME_PATH' , get_template_directory() );
    define( 'PAGE_BASENAME' , get_current_basename() );
    define( 'FIRST_VISIT' , is_first_time());
	define( 'HOME_URL' , str_replace('http:', '', get_permalink(get_page_by_path('home'))) );

    // Load custom post types
    require_once (TEMPLATEPATH . '/custom-post-types/CPT.php');
    //require_once (TEMPLATEPATH . '/custom-post-types/media.cpt.php');
    require_once (TEMPLATEPATH . '/custom-post-types/careers.cpt.php');

    // Add theme supports
    add_theme_support('post-thumbnails');

    // Register navigation
    register_nav_menu( 'main-nav' , 'Main Menu' );
    register_nav_menu( 'footer-nav' , 'Footer Menu' );

    // Add image sizes
    add_image_size('small', 500 , 0 , false );
    //add_image_size('content-tile-1x', 540 , 500 , true );
    //add_image_size('content-tile-2x', 1080 , 1000 , true );

    // Set default image link type
    update_option('image_default_link_type', 'file');

    // Set JPG quality for image resizing
    add_filter( 'jpeg_quality', 'set_jpg_quality' );
    function set_jpg_quality( $quality ) { return 95; }

    // Disabled admin bar
    add_filter('show_admin_bar', '__return_false');

    // Load stylesheets and javascript files
    add_action('init', 'theme_init');

    // Initilization function
    function theme_init() {

        // Load stylesheets and javascript files
        load_scripts_and_styles();

        // Create options page
        if( function_exists('acf_add_options_page') ) {

        	$options = acf_add_options_page(array(
        		'page_title' 	=> 'Site Options',
        		'menu_title' 	=> 'Site Options',
        		'menu_slug' 	=> 'options',
        		'capability' 	=> 'edit_posts',
                'parent_slug'   => 'options-general.php',
                'position'      => 0,
        		'redirect' 	=> false
        	));

        }

        // hide stuff we don't need
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'feed_links');

        // Remove the REST API endpoint.
        remove_action('rest_api_init', 'wp_oembed_register_route');

        // Turn off oEmbed auto discovery.
        // Don't filter oEmbed results.
        remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

        // Remove oEmbed discovery links.
        remove_action('wp_head', 'wp_oembed_add_discovery_links');

        // Remove oEmbed-specific JavaScript from the front-end and back-end.
        remove_action('wp_head', 'wp_oembed_add_host_js');

        add_editor_style();

    }


add_filter( 'custom_menu_order', 'wpse_73006_submenu_order' );

function wpse_73006_submenu_order( $menu_ord )
{
    global $submenu;

    $site_options = [];
    $site_options_key = '';

    foreach($submenu as $slug => $items) :

        if($slug == 'options-general.php') :

            foreach($items as $order => $item) :

                if($item[0] == 'Site Options') :
                    $site_options = $item;
                    $site_options_key = $order;
                endif;

            endforeach;

            unset($submenu[$slug][$site_options_key]);

            $submenu[$slug] = array_merge([$site_options], $submenu[$slug]);

        endif;

    endforeach;

    //print_r($submenu);

    // Enable the next line to see all menu orders
    //echo '<pre>'.print_r($submenu,true).'</pre>';
    //
    // $arr = array();
    // $arr[] = $submenu['edit.php'][5];     //my original order was 5,10,15,16,17,18
    // $arr[] = $submenu['edit.php'][10];
    // $arr[] = $submenu['edit.php'][18];
    // $arr[] = $submenu['edit.php'][17];
    // $arr[] = $submenu['edit.php'][15];
    // $arr[] = $submenu['edit.php'][16];
    // $submenu['edit.php'] = $arr;

    return $menu_ord;
}


    function load_scripts_and_styles() {

        if( ! is_admin() && ! in_array($_SERVER['PHP_SELF'], ['/wp-login.php', '/wp-register.php'])) :

            // Add stylesheets
            $style_version = md5(file_get_contents(THEME_PATH . '/dist/bundle.css'));
            wp_enqueue_style( 'main_styles' , THEME_URL . '/dist/bundle.css' , FALSE , $style_version);

            // Load scripts
            add_action('wp_enqueue_scripts', 'load_scripts', 0);

            // Replace jQuery with Google CDN version
            function load_scripts() {

                $jq_in_footer = false;


                // Load main scripts
                wp_enqueue_script( 'main_script' , THEME_URL . '/dist/main.js' , [] , md5(file_get_contents(THEME_PATH . '/dist/main.js')) , TRUE );

                wp_localize_script( 'main_script', '_global', array(
                    'ajaxurl' => admin_url( 'admin-ajax.php' ),
                    'site_name' => get_bloginfo( 'name' ),
                    'dist_path' => get_stylesheet_directory_uri() . '/dist/',
                    'map_menu' => get_field('map_menu_pages', 'option')
                ));


                //Load react related scripts only on "payment method type" template
                if(is_page_template('page-payment_method_type.php') || is_page_template('page-payment_method.php')){
                    $args = array(
                        'post_type' => 'page',
                        'posts_per_page'=> -1,
                        'post_status' => 'publish',
                        'post_parent' => wp_get_post_parent_id(get_the_ID())
                    );


                    $loop = new WP_Query($args);
                    $result = array();

                    while ( $loop->have_posts() ) : $loop->the_post();

                      $path = parse_url(get_the_permalink());
                      $path = $path['path'];

                      $result[] = [
                      'id' => get_the_ID(),
                      'title' => html_entity_decode(get_the_title()),
                      'permalink' => get_the_permalink(),
                      'path' => $path
                      ];
                    endwhile;

                    wp_reset_query();

                    // React
                    wp_enqueue_script('comp', THEME_URL . '/dist/infogcomp.js', [], null, true);
                    wp_localize_script( 'comp', '_data', array(
                      'infog' => $result,
                    ));
                }

            }

        endif;

    }

    function payment_method() {

        //check_ajax_referrer();

        $post_object = get_page_by_path($_POST['path']);

        $args = array(
            'post_type' => 'page',
            'p'=> $post_object->ID
        );

        $loop = new WP_Query($args);

        if ( $loop->have_posts() ) {
            $belowTilesVideo = [];
            $belowTilesImageGallery = [];
            while ( $loop->have_posts() ) : $loop->the_post();

                ob_start();
                get_template_part('parts/infog', 'tiles');
                $infogTilesHtml = ob_get_contents();
                ob_end_clean();

                ob_start();
                while ( have_rows('below_tiles') ) : the_row();
                    if( get_row_layout() == 'below_tiles_list' ):
                        get_template_part( 'parts/below_tiles', 'list' );
                    elseif( get_row_layout() == 'below_tiles_video' ):
                        get_template_part( 'parts/below_tiles', 'video' );
                        $belowTilesVideo[] = [
                                            'image'=> get_sub_field('thumbnail'),
                                            'video_url'=> get_sub_field('video_url'),
                                            ];
                    elseif( get_row_layout() == 'below_tiles_image_gallery' ):
                        get_template_part( 'parts/below_tiles', 'image_gallery' );
                        $belowTilesImageGallery [] = get_sub_field('images');
                    endif;
                endwhile;
                $belowTilesHtml = ob_get_contents();
                ob_end_clean();

            endwhile;

            wp_reset_postdata();
        } else {
             wp_send_json([]);
        }

        wp_send_json([
            'infogTilesHtml' => $infogTilesHtml,
            'belowTilesHtml' => $belowTilesHtml,
            'belowTilesVideo' => $belowTilesVideo,
            'belowTilesImageGallery' =>$belowTilesImageGallery
        ]);
    }

    add_action('wp_ajax_nopriv_payment_method', 'payment_method');
    add_action('wp_ajax_payment_method', 'payment_method');


    //Redirect to first child
    function template_redirect_to_first_child()
    {
        if( is_page_template('page-payment_method_type.php') )
        {

            $args = array(
                'post_type' => 'page',
                'posts_per_page'=> 1,
                'post_status' => 'publish',
                'post_parent' => get_the_ID()
            );

            $loop = new WP_Query($args);

            while ( $loop->have_posts() ) : $loop->the_post();
            wp_redirect(get_permalink());
            endwhile;
            wp_reset_postdata();
        }
    }
    add_action( 'template_redirect', 'template_redirect_to_first_child' );


    // Rename WP admin menu items
    /*add_action( 'admin_menu', 'change_wp_admin_menu_labels' );

    function change_wp_admin_menu_labels() {
        global $menu;
        global $submenu;

        // Media library
        $menu[10][0] = 'Library';
    }*/

    // Disable admin menus
    add_action( 'admin_menu', 'remove_menus');
    function remove_menus() {
        // remove_menu_page( 'index.php' );                  //Dashboard
        //remove_menu_page( 'edit.php' );                   //Posts
        // remove_menu_page( 'upload.php' );                 //Media
        // remove_menu_page( 'edit.php?post_type=page' );    //Pages
        remove_menu_page( 'edit-comments.php' );          //Comments
        //remove_menu_page( 'themes.php' );                 //Appearance
        // remove_menu_page( 'plugins.php' );                //Plugins
        // remove_menu_page( 'users.php' );                  //Users
        // remove_menu_page( 'tools.php' );                  //Tools
        // remove_menu_page( 'options-general.php' );        //Settings
        //remove_menu_page( 'edit.php?post_type=acf-field-group' );        //ACF

        add_menu_page('Menus', 'Menus', 'edit_themes', 'nav-menus.php', '', 'dashicons-menu', 60);

        add_submenu_page( 'options-general.php', 'Themes', 'Themes', 'manage_options', 'themes.php');
        //add_submenu_page( 'options-general.php', 'Custom Fields', 'Custom Fields', 'manage_options', 'edit.php?post_type=acf-field-group');

    }

    // Disable emoji support
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );

    // Increase excerpt length
    add_filter('excerpt_length', 'custom_excerpt_length', 999);
    function custom_excerpt_length( $length ) { return 100; }

    // Filter HTML in Wordpress text widget
    add_filter('widget_text', 'filter_text_widget');
    function filter_text_widget( $text ) { return apply_filters('the_content', $text); }

    // Remove hardcoded width/height attrs from post thumbnails
    add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
    function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }

    // Remove dimension attributes from img tags in content
    add_filter('the_content', 'remove_img_dimensions', 10);
    function remove_img_dimensions($html) {
        $html = preg_replace('/(width|height)=["\']\d*["\']\s?/', "", $html);
        return $html;
    }

    // Add nav menu item names to links
    add_filter( 'nav_menu_css_class', 'add_nav_item_name_class', 10, 2 );
    function add_nav_item_name_class( $classes , $item ) {
        $new_class = strtolower( preg_replace("/[^A-Za-z0-9 ]/", '-', $item->title) );
        $new_class = str_replace(' ', '-', $new_class);
        //$classes[] = $new_class;
        $classes[] = $new_class.'-link';
        return $classes;
    }

    // Add post/page slug to body classes (eg. page-home)
    add_filter( 'body_class', 'add_slug_body_class' );
    function add_slug_body_class( $classes ) {
        global $post;
        if (isset($post)) :
            $classes[] = $post->post_type . '-' . $post->post_name;

            if($post->post_parent != 0) :
                $parent = get_post($post->post_parent);
                $classes[] = $post->post_type . '-parent-' . $parent->post_name;
            endif;

        endif;

        return $classes;
    }

    add_filter('upload_mimes', 'add_svg_mime_type');
    function add_svg_mime_type($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    function my_mce_buttons_2( $buttons ) {
    	array_unshift( $buttons, 'styleselect' );
    	return $buttons;
    }
    // Register our callback to the appropriate filter
    add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

    add_filter( 'tiny_mce_before_init', 'tiny_mce_insert_formats' );
    function tiny_mce_insert_formats( $init_array ) {

    	$style_formats = [
    		[
    			'title' => 'Intro',
    			'block' => 'p',
    			'classes' => 'intro',
    			'wrapper' => false,
    		],
            [
    			'title' => 'Two Column Text',
    			'block' => 'div',
    			'classes' => 'content__text-two-columns',
    			'wrapper' => true,
    		]
    	];

    	$init_array['style_formats'] = json_encode( $style_formats );

    	return $init_array;
    }

    add_filter( 'auto_update_plugin', '__return_true' );

    add_filter( 'the_content_more_link', 'remove_read_more_link' );
    function remove_read_more_link() {
        return '';
    }

    // filter the Gravity Forms button type
    add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
    function form_submit_button( $button, $form ) {
        return '
            <button type="submit" class="button" id="gform_submit_button_'.$form['id'].'">
                <div class="button__inner">
                    <div class="whitebg"></div>
                    <span class="button__label">Submit</span>
                </div>
            </button>';
    }

    add_filter( 'gform_init_scripts_footer', '__return_true' );
