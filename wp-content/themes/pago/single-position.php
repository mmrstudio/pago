<?php

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        get_header();

        global $wp;
        $current_url = home_url(add_query_arg(array(), $wp->request));

        $share_links = [
            'facebook' => [
                'name' => 'Facebook',
                'url' => 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($current_url),
            ],

            'linkedin' => [
                'name' => 'LinkedIn',
                'url' => 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($current_url),
            ],
            'email' => [
                'name' => __('Email', 'pago'),
                'url' => 'mailto:?subject=' . wp_title('|', false) . '&body=' . $current_url,
            ]
        ];

?>

<div class="sections">



    <section class="section section--content-row content">

        <div class="section__container">
			<a href="/careers/" class="backtobt">< <?php echo __('Back to Careers', 'pago'); ?></a>
            <div class="section__inner">
								<h1 class="heading1--red"><?php the_title(); ?></h1>
                <div class="content__columns">

                    <div class="content__columns__column content__columns__column--sidebar" data-width="full">

                        <div class="component component--text-block">
                            <?php the_content(); ?>
                        </div>

                        <div class="component component--sidebar">

                            <div class="component--sidebar__callout">
                                <?php echo get_field('sidebar'); ?>
                                <a href="/careers/apply?p=<?php echo urlencode(get_the_title()); ?>" class="button">
																				<div class="whitebg"></div>
                                        <span class="button__label"><?php echo __('Apply Now', 'pago'); ?></span>

                                </a>
                            </div>

                            <div class="component--sidebar__share component--share">
                                <span><?php echo __('Share this role', 'pago'); ?></span>
                                <ul class="social-icons">
                                    <?php foreach($share_links as $network => $link) : ?>
                                    <li><a href="<?php echo $link['url']; ?>" class="icon-<?php echo $network; ?>" <?php if($network != 'email') : ?>target="_blank"<?php endif; ?>><?php echo $link['name']; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                            <nav class="component--sidebar__nav">
                                <div class="component--sidebar__nav__prev"><?php previous_post_link('%link', '< ' . __('Previous', 'pago')); ?></div>
                                <div class="component--sidebar__nav__next"><?php next_post_link('%link', __('Next', 'pago') . ' >'); ?></div>
                            </nav>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

</div>

<?php

    endwhile; // end loop

    get_footer();

?>
