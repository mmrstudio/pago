<?php
        get_header();

?>

<div class="sections">

	<section class="section section--page-heading content">

        <div class="section__container">

            <div class="section__inner">
                <h1>404</h1>
            </div>

        </div>

    </section>


	<section class="section section--content-row content">

        <div class="section__container">

            <div class="section__inner">

                <div class="section__title">
                    <h2>The page you are looking for cannot be found!</h2>
                </div>
  
            </div>

            
        </div>

    </section>


</div>

<?php

    get_footer();

?>
