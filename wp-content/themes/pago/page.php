<?php

    // start loop
	if(have_posts()) while (have_posts()) : the_post();

        get_header();

        //print_r(get_field('sections')); exit;

?>

<div class="sections">
	<section class="section section--content-row  content" >

			<div class="section__container">

					<div class="section__inner">
						<h1 class="heading1--small"><?php the_title(); ?></h1>


							<div class="component component--text-block">

								<?php the_content();?>
							</div>





					</div>

			</div>

	</section>





</div>

<?php

    endwhile; // end loop

    get_footer();

?>
