<div  class="section section--v1 section--overlap">
    <div class="section__container">
        <div class="indented">
            <div class="col-md-12 col-lg-4 indented__col">
            <?php the_sub_field('column_1'); ?>
            </div>

            <div class="col-md-6 col-lg-4 indented__col">
            <?php the_sub_field('column_2'); ?>
            </div>

            <div class="col-md-6 col-lg-4 indented__col">
            <?php the_sub_field('column_3'); ?>
            </div>
        </div>
    </div>
</div>