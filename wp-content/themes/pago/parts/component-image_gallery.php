
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--image-gallery <?php echo set_visibility_class($component['hide_on']); ?>">

            <div class="component--image-gallery__inner">
            
                <div class="component--image-gallery__slider">
                    <?php foreach($component['gallery'] as $image) : ?>
                    <div  title="<?php echo $image['title']; ?>" class="component--image-gallery__slider__cell" style="background-image: url(<?php echo acf_image($image, false, false); ?>);"></div>
                    <?php endforeach; ?>
                </div>

            </div>

            <?php acf_component_content($component); ?>
            <?php acf_component_buttons($component); ?>

        </div>

    </div>
