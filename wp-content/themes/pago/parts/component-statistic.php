
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--statistic">
            <div class="component--statistic__image">
                <?php echo acf_image($component['image'], 'large', true); ?>
            </div>
            <div class="component--statistic__label"><?php echo $component['label']; ?></div>
            <div class="component--statistic__value"><?php echo $component['value']; ?></div>
            <?php if($component['value_extra']) : ?>
            <div class="component--statistic__value-extra"><?php echo $component['value_extra']; ?></div>
            <?php endif; ?>
            <?php acf_component_buttons($component); ?>
        </div>

    </div>
