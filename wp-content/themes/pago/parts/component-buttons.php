
    <?php if($component['buttons']) : ?>
    <div class="component__buttons button__group <?php echo set_button_alignment_class($component['alignment']); ?>">
        <?php foreach($component['buttons'] as $button) : ?>
        <a href="<?php echo link_url($button['link_type'], $button['link_internal'], $button['link_external']); ?>" class="button <?php echo $button['icon'] != 'none' ? 'button--icon' : ''; ?> " target="<?php echo link_target($button['link_target']); ?>" >
            <div class="button__inner">
                <?php if($button['icon'] != 'none') : ?>
                <div class="button__icon <?php echo $button['icon']; ?>"></div>
                <?php endif; ?>
                <span class="button__label"><?php echo $button['label']; ?></span>
            </div>
        </a>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
