
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--image-single <?php echo set_visibility_class($component['hide_on']); ?> <?php echo set_image_type($component['image_type']); ?>">

            <?php echo acf_image($component['image'], false, true); ?>

            <?php acf_component_content($component); ?>
            <?php acf_component_buttons($component); ?>

        </div>

    </div>
