
    <section class="section go-cashless content">
     <div class="go-cashless-static"></div>

        <div class="section__container">

            <div class="section__inner">


                <div class="content__columns">


                    <div class="content__columns__column" style="margin-bottom: 0; position: relative;" data-width="third">
                    
                        <div class="go-cashless-anim">
                            <div class="go-cashless-anim__image go-cashless-anim__image--circle-big"></div>
                            <div class="go-cashless-anim__image go-cashless-anim__image--circle-small"></div>
                            <div class="go-cashless-anim__image go-cashless-anim__image--hand-bottom"></div>
                            <div class="go-cashless-anim__image go-cashless-anim__image--hand-top"></div>
                        </div>

                    </div>


                    <div class="go-cashless__points content__columns__column" data-width="two_thirds">

                        <div style="width: 100%;">

                            <div class="content__columns">
                            <?php if(get_sub_field('section_title')) : ?>
                                <h2 class="go-cashless__h2"><?php echo get_sub_field('section_title'); ?></h2>
                            <?php endif; ?>

                                <?php foreach(get_sub_field('points') as $point) : ?>
                                <div class="content__columns__column go-cashless__point" data-width="third">
                                    <div class="go-cashless__point-infographic">
                                        <div  class="go-cashless__point-image" style="background-image: url(<?php echo acf_image($point['image'], false); ?>);"></div>
                                    </div>
                                    <h3 class="go-cashless__point-h3"><?php echo $point['point_title']; ?></h3>
                                    
                                </div>
                                <?php endforeach; ?>


                                <div class="go-cashless__button content__columns__column" data-width="full">
                                    <a href="<?php echo get_sub_field('button_link'); ?>" class="button" target="_self">
                                        <div class="button__inner">
                                            <span class="button__label"><?php echo get_sub_field('button_text'); ?></span>
                                        </div>

                                    </a>
                                </div>
     

                            </div>

                        </div>

                    </div>

                    

                </div>

            </div>

        </div>

    </section>
