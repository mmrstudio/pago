
    <?php

        $video_data = [
            'type' => 'tabs',
            'videos' => [],
        ];

        foreach($component['videos'] as $tab) :

            $video_data['videos'][] = [
                'videoType' => $tab['video_type'],
                'videoUrl' => $tab['video_type'] == 'url' ? $tab['video_url'] : $tab['youtube_url'],
                'videoId' => $tab['video_type'] == 'youtube' ? parse_youtube_id($tab['youtube_url']) : false,
                'embedUrl' => $tab['video_type'] == 'youtube' ? youtube_embed_url($tab['youtube_url']) : false,
                'thumbnail' => acf_image($tab['video_thumbnail'], 'large', false),
                'tabLabel' => $tab['label'],
                'tabIcon' => $tab['icon'],
            ];

        endforeach;

    ?>

    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--video-tabs" data-video-data='<?php echo json_encode($video_data); ?>'></div>

        <?php acf_component_content($component); ?>
        <?php acf_component_buttons($component); ?>

    </div>
