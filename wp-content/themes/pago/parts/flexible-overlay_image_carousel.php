
    <section class="section section--content-row content" <?php $background = get_sub_field('background_colour'); echo $background != 'none' ? 'data-background="' . $background . '"' : ''; ?>  <?php echo (get_sub_field('hide_separator') && $background == 'none') ? 'data-hide-separator' : '' ?>>

        <div class="section__container">

            <div class="section__inner">

                <?php if(get_sub_field('section_title')) : ?>
                <div class="section__title">
                    <h2><?php echo get_sub_field('section_title'); ?></h2>
                </div>
                <?php endif; ?>

                

                

                <?php // Start tiles ?>


                <?php if( have_rows('images') ): ?>

                <div class=" overlay_image_carousel">
                <?php while( have_rows('images') ): the_row(); 

                    $image = get_sub_field('image');

                    ?>
                    <div class="component--image-overlay-carousel">
                        <div class="component component--image-overlay">

                            <a href="<?php echo link_url(get_sub_field('link_type'), get_sub_field('link_internal'), get_sub_field('link_external')); ?>" class="component--image-overlay__overlay">
                                <div class="component--image-overlay__overlay__inner">

                                    <h4><?php echo get_sub_field('heading'); ?></h4>
                                    <hr>
                                    <?php echo get_sub_field('text'); ?>
                                    <p class="component--image-overlay__link">
                                        <strong><?php echo get_sub_field('link_text'); ?></strong>
                                    </p>
                                    
                                </div>
                            </a>

                            <div title="<?php echo $image['title']; ?>" class="component--image-overlay__image" style="background-image: url(<?php echo $image['url']; ?>);">
                            </div>

                            
                        </div>
                    </div>

                <?php endwhile; ?>
                </div>

                <?php endif; ?>




                <?php // End tiles ?>



                

                <?php if(get_sub_field('trailing_text')) : ?>
                <div class="section__trailing-text">
                    <?php echo get_sub_field('trailing_text'); ?>
                </div>
                <?php endif; ?>

            </div>

        </div>

    </section>
