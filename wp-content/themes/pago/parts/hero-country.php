<div class="country-hero">

    <style>
        .country-hero-image {
            background-image: url(<?php echo $post_header_mobile_image; ?>);
        }

        @media (min-width: 768px) {
           .country-hero-image {
                background-image: url(<?php echo $post_header_desktop_image; ?>);
            } 
        }
    </style>

    <div class="country-hero-image"><h1 class="no-margin"><?php echo $post_title; ?></h1></div>

    <div class="country-menu">

        <div class="country-menu__caption">
            <div class="country-menu__caption-text"> <?php _e('Choose payment method types') ?></div>
        </div>

        <ul class="country-menu__links">
            
            <?php
            $args = array(
                'post_type' => 'page',
                'posts_per_page'=> -1,
                'post_status' => 'publish',
                'post_parent' => $post_parent //Set at the template
            );

            $loop = new WP_Query($args);

            foreach ($loop->posts as $key => $post) {?>
                 
                <li><a href="<?php echo get_permalink($post->ID); ?>" class="country-menu__link <?php echo ($post->ID == $post_active)?'country-menu__link--active':''; ?> "><?php echo $post->post_title; ?></a></li>

                <?php
            }
            wp_reset_query();
            ?>
        </ul>

    </div>
    
</div>