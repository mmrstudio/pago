
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--text-block">

            <?php echo $component['text_block_content']; ?>

            <?php acf_component_buttons($component); ?>

        </div>

    </div>
