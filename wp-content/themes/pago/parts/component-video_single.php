
    <?php

        $video_data = [
            'type' => 'single',
            'videos' => [
                [
                    'videoType' => $component['video_type'],
                    'videoUrl' => $component['video_type'] == 'url' ? $component['video_url'] : $component['youtube_url'],
                    'videoId' => $component['video_type'] == 'youtube' ? parse_youtube_id($component['youtube_url']) : false,
                    'embedUrl' => $component['video_type'] == 'youtube' ? youtube_embed_url($component['youtube_url']) : false,
                    'thumbnail' => acf_image($component['video_thumbnail'], 'large', false),
                ],
            ],
        ];

    ?>

    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--video-single" data-video-data='<?php echo json_encode($video_data); ?>'></div>

        <?php acf_component_content($component); ?>
        <?php acf_component_buttons($component); ?>

    </div>
