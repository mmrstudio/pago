
    <?php

        $show = get_sub_field('show_positions');
        $sort = get_sub_field('sort');
        $sort = explode('_', $sort);
        $post__in = false;

        switch($show) :

            case 'all' :
                $posts_per_page = -1;
            break;

            case 'latest' :
                $posts_per_page = get_sub_field('how_many');
            break;

            case 'specific' :
                $posts_per_page = -1;
                $post__in = get_sub_field('specific_positions');
            break;

        endswitch;

        $get_positions_query = [
            'post_type' => 'position',
            'posts_per_page' => $posts_per_page,
            'orderby' => $sort[0],
            'order' => $sort[1],
        ];

        if($post__in) $get_positions_query['post__in'] = $post__in;

        //print_r($get_positions_query); exit;

        $get_positions = new WP_Query($get_positions_query);

        //print_r($get_positions); exit;

        $positions = [];

        if($get_positions->have_posts()) :
            while($get_positions->have_posts()) : $get_positions->the_post();

                $positions[] = [
                    'title' => get_the_title(),
                    'description' => strip_tags(get_the_content()),
                    'permalink' => get_permalink(),
                ];

                wp_reset_query();
            endwhile;
        endif;

        //print_r($positions); exit;

    ?>

    <section class="section section--content-row section--careers content" <?php $background = get_sub_field('background_colour'); echo $background != 'none' ? 'data-background="' . $background . '"' : ''; ?>  <?php echo (get_sub_field('hide_separator') && $background == 'none') ? 'data-hide-separator' : '' ?>>

        <div class="section__container">

            <div class="section__inner">

                <?php if(get_sub_field('section_title')) : ?>
                <div class="section__title">
                    <h2><?php echo get_sub_field('section_title'); ?></h2>
                </div>
                <?php endif; ?>

                <div class="content__columns">

                    <?php foreach($positions as $position) : ?>
                    <div class="content__columns__column section--careers__position" data-width="full">

                        <div class="component component--text-block">

                            <h3><?php echo $position['title']; ?></h3>
                            <p><?php echo $position['description']; ?></p>

                            <div class="component__buttons button__group">

                                <a href="<?php echo $position['permalink']; ?>" class="button">
                                    <div class="button__inner">
                                        <div class="button__icon icon-document"></div>
                                        <span class="button__label">Read Full Description</span>
                                    </div>
                                </a>

                                <a href="/careers/apply?p=<?php echo urlencode($position['title']); ?>" class="button">
                                    <div class="button__inner">
                                        <div class="button__icon icon-document-edit"></div>
                                        <span class="button__label">Apply Now</span>
                                    </div>
                                </a>

                            </div>

                        </div>

                    </div>
                    <?php endforeach; ?>

                </div>

                <?php if(get_sub_field('trailing_text')) : ?>
                <div class="section__trailing-text">
                    <?php echo get_sub_field('trailing_text'); ?>
                </div>
                <?php endif; ?>

            </div>

        </div>

    </section>
