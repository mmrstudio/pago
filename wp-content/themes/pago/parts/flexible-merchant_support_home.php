
    <section class="section section--merchant-support-home" id="merchantSupport">

        <div class="section--merchant-support-home__inner">

            <div class="section--merchant-support-home__wrap">

                <div class="section--merchant-support-home__image">

                    <?php echo acf_image(get_sub_field('image'), false, true); ?>

                    <div class="section--merchant-support-home__title section--merchant-support-home__title--image">
                        <span class="section--merchant-support-home__title__lrg section--merchant-support-home__title__lrg--white">24/7</span>
                        <span class="section--merchant-support-home__title__sml section--merchant-support-home__title__sml--white">Merchant<br>Support</span>
                    </div>

                </div>

                <div class="section--merchant-support-home__content">

                    <div class="section--merchant-support-home__title section--merchant-support-home__title--content">
                        <span class="section--merchant-support-home__title__lrg">24/7</span>
                        <span class="section--merchant-support-home__title__sml">Merchant<br>Support</span>
                    </div>

                    <?php echo get_sub_field('introduction'); ?>

                    <div class="component__buttons button__group">
                        <a href="#merchant-support" class="button button--icon">
                            <div class="button__inner">
                                <div class="button__icon icon-comment"></div>
                                <span class="button__label">Contact Us</span>
                            </div>
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </section>
