
<div class="responsive-nav">

    <div class="responsive-nav__inner">

        <nav class="responsive-nav__nav" id="responsiveNav">

            <?php output_nav('main-nav', 2, 'responsive-nav__nav__menu'); ?>

        </nav>

    </div>

</div>
<div class="responsive-nav__dismiss dismiss-nav" id="responsiveNavDismiss"></div>
