
    <ul class="social-icons">
        <?php if($social_facebook = get_field('social_facebook', 'option')) : ?><li><a href="<?php echo $social_facebook; ?>" class="icon-facebook" target="_blank">Facebook</a></li><?php endif; ?>
        <?php if($social_twitter = get_field('social_twitter', 'option')) : ?><li><a href="<?php echo $social_twitter; ?>" class="icon-twitter" target="_blank">Google Plus</a></li><?php endif; ?>
        <?php if($social_linkedin = get_field('social_linkedin', 'option')) : ?><li><a href="<?php echo $social_linkedin; ?>" class="icon-linkedin" target="_blank">LinkedIn</a></li><?php endif; ?>
        <?php if($social_youtube = get_field('social_youtube', 'option')) : ?><li><a href="<?php echo $social_youtube; ?>" class="icon-youtube" target="_blank">YouTube</a></li><?php endif; ?>
    </ul>
