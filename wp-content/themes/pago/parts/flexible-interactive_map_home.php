
    <?php

        $content = [
            'title' => get_sub_field('map_title'),
            'introduction' => get_sub_field('map_introduction'),
            'button_link' => get_sub_field('map_button_link'),
            'button_text' => get_sub_field('map_button_text'),
            'china_link' => get_sub_field('china_link'),
            'thailand_link' => get_sub_field('thailand_link'),
            'indonesia_link' => get_sub_field('indonesia_link'),
        ];

    ?>

    <section class="section section--interactive-map--home" data-map-content='<?php echo json_encode($content); ?>'></section>
