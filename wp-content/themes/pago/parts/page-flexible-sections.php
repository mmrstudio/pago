
    <div class="content">

        <?php

            // flexible content areas
            acf_flexible('sections', function($i, $layout) { //echo $layout;

                // load template
                get_template_part('parts/flexible', $layout);

            }); // end acf_flexible

        ?>

    </div>
