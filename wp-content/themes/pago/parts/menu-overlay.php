<?php require_once get_template_directory() . '/lib/wp-bootstrap-navwalker.php'; ?>



<div class="overlay-back"></div>
<div class="overlay-menu">
   <div class="overlay-menu__content">
        <a href="#" class="overlay-menu__close"></a>

        <?php
            wp_nav_menu( [
                'menu'              => 'main-nav',
                'theme_location'    => 'main-nav',
                'depth'             => 2,
                'container'         => '',
                //'container_class'   => '',
                //'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'overlay-menu__l0-links',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
                ]
            );
        ?>
        <?php /* <ul class="overlay-menu__l1-links">
           <li>
               <a href="" class="overlay-menu__l1-link">About PaGO</a>
               <ul class="overlay-menu__l2-links">
                   <li><a href="" class="overlay-menu__l2-link">What is Pago?</a></li>
                   <li><a href="" class="overlay-menu__l2-link">24/7 Merchant Support</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Careers</a></li>
                   <li><a href="" class="overlay-menu__l2-link">FAQs</a></li>
               </ul>
           </li>
           <li>
               <a href="" class="overlay-menu__l1-link overlay-menu__l1-link--active">Country Specific solutions</a>
               <ul class="overlay-menu__l2-links">
                   <li><a href="" class="overlay-menu__l2-link">Argentina</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Brazil</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Chile</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Colombia</a></li>
                   <li><a href="" class="overlay-menu__l2-link overlay-menu__l2-link--active">Mexico</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Peru</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Uruguay</a></li>
               </ul>
           </li>
           <li>
               <a href="" class="overlay-menu__l1-link">Payment Solutions</a>
               <ul class="overlay-menu__l2-links">
                   <li><a href="" class="overlay-menu__l2-link">Prepaid Card Solution</a></li>
                   <li><a href="" class="overlay-menu__l2-link">Multi-channel Payment Gateway</a></li>
               </ul>
           </li>
           <li><a href="" class="overlay-menu__l1-link">Contact</a></li>
               </ul> */ ?>
      <?php /*
       <p class="overlay-menu__copyright"><?php echo __('Copyright', 'pago'); ?> &copy;2017 Pago.
           <?php echo __('Pago is a Sappaya company', 'pago'); ?>.
           <?php echo __('Design by', 'pago'); ?> <a href="#">MMR</a>. | <a href="#"><?php echo __('Privacy Policy', 'pago'); ?></a></p>
       */ ?>
        <ul class="overlay-menu__social">
            <?php if($social_facebook = get_field('social_facebook', 'option')) : ?><li><a href="<?php echo $social_facebook; ?>" class="icon-facebook overlay-menu__social-link" target="_blank" title="Pago on Facebook"></a></li><?php endif; ?>
            <?php if($social_twitter = get_field('social_twitter', 'option')) : ?><li><a href="<?php echo $social_twitter; ?>" class="icon-twitter overlay-menu__social-link" target="_blank" title="Pago on Twitter"></a></li><?php endif; ?>
            <?php if($social_linkedin = get_field('social_linkedin', 'option')) : ?><li><a href="<?php echo $social_linkedin; ?>" class="icon-linkedin overlay-menu__social-link" target="_blank" title="Pago on LinkedIn"></a></li><?php endif; ?>
            <?php if($social_youtube = get_field('social_youtube', 'option')) : ?><li><a href="<?php echo $social_youtube; ?>" class="icon-youtube overlay-menu__social-link" target="_blank" title="Pago on YouTube"></a></li><?php endif; ?>
            <?php if($social_instagram = get_field('social_instagram', 'option')) : ?><li><a href="<?php echo $social_instagram; ?>" class="icon-instagram overlay-menu__social-link" target="_blank" title="Pago on Instagram"></a></li><?php endif; ?>
        </ul>
   </div>
</div>
