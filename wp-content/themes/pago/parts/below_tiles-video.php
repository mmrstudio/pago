<div  class="section section--v2">
    <div class="section__container">
        <div class="indented">
            <div class="col-lg-3 indented__col">
                <?php the_sub_field('title'); ?>
            </div>
            <div class="col-lg-8 indented__col country-video-col">
                <div class="country-video right-frame">
                    <a style="background-image:url(<?php the_sub_field('thumbnail'); ?>); " class="country-video__link" href="<?php the_sub_field('video_url'); ?>"></a>
                </div>
            </div>
        </div>
    </div>
</div>