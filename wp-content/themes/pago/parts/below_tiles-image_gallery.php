<div  class="section section--v2">
    <div class="section__container">
        <div class="indented">
            <div class="col-lg-3 indented__col">
                <?php the_sub_field('title'); ?>
            </div>
            <div class="col-lg-8 indented__col country-image-gallery-col">

                <div class="country-image-gallery right-frame">
                    <?php 
                        $images = get_sub_field('images');
                        $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)

                        if( $images ): ?>
                            <ul>
                                <?php foreach( $images as $image ): ?>
                                    <li>
                                        <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>