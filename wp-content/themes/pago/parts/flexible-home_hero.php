
    <?php

        $hero_tiles = get_sub_field('tiles');
        shuffle($hero_tiles);

    ?>

    <section class="home-hero">

        <div class="home-hero-header">
            <div class="home-hero-header__logo"></div>
            <h1 class="home-hero-header__title"><?php echo get_sub_field('hero_title'); ?></h1>
        </div>

        <div class="home-hero-content">
            <div class="home-hero-content__wrap">
                <div class="home-hero-content__inner">

                    <div class="home-hero-content__tile-wrap">
                        <div class="home-hero-content__tile-wrap__col">

                            <?php $t=1; if($hero_tiles) : foreach($hero_tiles as $tile) : ?>
                            <div class="home-hero-content__tile" data-tile-num="<?php echo $t; ?>">
                                <a title="<?php echo $tile['title']; ?>" href="<?php echo $tile['link']; ?>" class="home-hero-content__tile__inner" style="background-image: url(<?php echo acf_image($tile['image'], 'large', false); ?>);">
                                    <div class="home-hero-content__tile__title"><?php echo $tile['title']; ?></div>
                                </a>
                            </div>

                            <?php if($t === 2) : ?>
                            </div><div class="home-hero-content__tile-wrap__col">
                            <?php endif; ?>

                            <?php $t++; endforeach; endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="home-hero-tap">
           <div class="home-hero-tap__circle home-hero-tap__circle--big"></div>
           <div class="home-hero-tap__circle home-hero-tap__circle--small"></div>
           <div class="home-hero-tap__hand" id="home-hero-tap__hand"></div>
        </div>

        <div class="home-hero-bg" style="background-image: url(<?php echo acf_image(get_sub_field('background_image'), false); ?>);"></div>
    </section>

    <?php // Menu to go under the hero ?>

    <div class="header__border header__main--home-hero">
        <div class="header__main">

            <a href="<?php echo site_url(); ?>" class="header__logo" title="<?php echo get_bloginfo('name'); ?>"></a>

            <nav class="header__nav nav nav--header">
                <?php output_nav('main-nav', 2, 'nav--header__menu', 'headerNav'); ?>
            </nav>

        </div>
    </div>
