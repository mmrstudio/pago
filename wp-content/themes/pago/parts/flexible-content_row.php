
    <section class="section section--content-row content" <?php $background = get_sub_field('background_colour'); echo $background != 'none' ? 'data-background="' . $background . '"' : ''; ?>  <?php echo (get_sub_field('hide_separator') && $background == 'none') ? 'data-hide-separator' : '' ?>>

        <div class="section__container">

            <div class="section__inner">

                <?php if(get_sub_field('section_title')) : ?>
                <div class="section__title">
                    <h2><?php echo get_sub_field('section_title'); ?></h2>
                </div>
                <?php endif; ?>

                <div class="content__columns">

                <?php
                    acf_flexible_components('components');
                ?>

                </div>

                <?php if(get_sub_field('trailing_text')) : ?>
                <div class="section__trailing-text">
                    <?php echo get_sub_field('trailing_text'); ?>
                </div>
                <?php endif; ?>

            </div>

            <?php if(($footer = get_sub_field('footer'))) : ?>
            <div class="component__footer">
            <?php foreach($footer as $footerElement) { ?>
            
                <?php if($footerElement == 'social') {?>
                    <ul class="component-footer-social">
                        <li class="component-footer-social__li">Share</li>
                        <li class="component-footer-social__li"><a class="component-footer-social__link icon-facebook" href="https://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank" title="Share on Facebook"></a></li>
                        <li class="component-footer-social__li"><a class="component-footer-social__link icon-twitter" href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>&hashtags=justtappit" target="_blank" title="Share on Twitter"></a></li>
                        <li class="component-footer-social__li"><a class="component-footer-social__link icon-linkedin" href="https://www.linkedin.com/shareArticle?url=<?php echo urlencode(get_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>" target="_blank" title="Share on Linkedin"></a></li>
                        <li class="component-footer-social__li"><a class="component-footer-social__link icon-email" href="mailto:&subject=<?php echo rawurlencode(get_the_title()); ?>&body=<?php echo rawurlencode(get_permalink()); ?>" title="Send as email"></a></li>
                    </ul>
                <?php }else if($footerElement == 'nav'){ ?>
                    <?php
                        global $post;
                        $pagelist = get_pages("child_of=".$post->post_parent."&parent=".$post->post_parent."&sort_column=menu_order&sort_order=asc");
                        $pages = array();
                        foreach ($pagelist as $page) {
                           $pages[] += $page->ID;
                        }

                        $current = array_search($post->ID, $pages);
                        $prevID = $pages[$current-1];
                        $nextID = $pages[$current+1];
                    ?>
                    <div class="component-footer-navigation">
                        <?php if (!empty($prevID)) { ?>
                        <a class="component-footer-navigation__link" href="<?php echo get_permalink($prevID); ?>" title="Previous: <?php echo get_the_title($prevID); ?>">< Previous</a>
                        <?php } ?>
                        | 
                        <?php if (!empty($nextID)) { ?>
                        <a class="component-footer-navigation__link" href="<?php echo get_permalink($nextID); ?>" title="Next: <?php echo get_the_title($nextID); ?>">Next ></a>
                        <?php } ?>
                    </div>
                <?php } ?>

            <?php } ?> 
            </div>
            <?php endif; ?>

        </div>

    </section>
