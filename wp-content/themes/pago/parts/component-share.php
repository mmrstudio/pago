
    <?php

        global $wp;
        $current_url = home_url(add_query_arg(array(), $wp->request));

        $share_networks = [
            'facebook' => [
                'name' => 'Facebook',
                'url' => 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($current_url),
            ],

            'linkedin' => [
                'name' => 'LinkedIn',
                'url' => 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($current_url),
            ],
            'email' => [
                'name' => 'Email',
                'url' => 'mailto:?subject=' . wp_title('|', false) . '&body=' . $current_url,
            ]
        ];

    ?>

    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--share">
            <span><?php echo $component['display_text']; ?></span>
            <ul class="social-icons">
                <?php foreach($component['networks'] as $network) : ?>
                <li><a href="<?php echo $share_networks[$network]['url']; ?>" class="icon-<?php echo $network; ?>" <?php if($network != 'email') : ?>target="_blank"<?php endif; ?>><?php echo $share_networks[$network]['name']; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>

    </div>
