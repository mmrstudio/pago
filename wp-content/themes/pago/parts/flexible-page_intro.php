
    <?php

        $media_type = get_sub_field('media_type');

        $component = false;
        $layout_template = false;

        switch($media_type) :

            case 'image' :

                $component = [
                    'template' => 'component-image_single.php',
                    'width' => 'two_thirds',
                    'hide_on' => false,
                    'image' => get_sub_field('image')
                ];

            break;

            case 'video' :

                $component = [
                    'template' => 'component-video_single.php',
                    'width' => 'two_thirds',
                    'hide_on' => false,
                    'video_type' => get_sub_field('video_type'),
                    'video_url' => get_sub_field('video_url'),
                    'youtube_url' => get_sub_field('youtube_url'),
                    'video_thumbnail' => get_sub_field('video_thumbnail'),
                ];

            break;

            case 'gallery' :

                $component = [
                    'template' => 'component-image_gallery.php',
                    'width' => 'two_thirds',
                    'hide_on' => false,
                    'gallery' => get_sub_field('gallery')
                ];

            break;

        endswitch;

        //print_r($component);

    ?>


    <section class="section section--page-intro" <?php $background = get_sub_field('background_colour'); echo $background != 'none' ? 'data-background="' . $background . '"' : ''; ?>  <?php echo (get_sub_field('hide_separator') && $background == 'none') ? 'data-hide-separator' : '' ?>>

        <div class="section__container">
            
            <div class="section__inner">
<?php if(get_sub_field('section_title')) : ?>
            <div class="section__title">
                <h2><?php echo get_sub_field('section_title'); ?></h2>
            </div>
            <?php endif; ?>
                

                <div class="content__columns section--page-intro__inner">

<?php

    if($component) :

        $layout_template = locate_template('parts/' . $component['template']);

        // load template
        if($layout_template) include($layout_template);

    endif;

?>

                    <div class="content__columns__column" data-width="third">

                        <div class="section--page-intro__content content">
                            <?php echo get_sub_field('content'); ?>
                        </div>

                    </div>

                </div>

            </div>
            
            <?php if(get_sub_field('trailing_text')) : ?>
            <div class="section__trailing-text">
                <?php echo get_sub_field('trailing_text'); ?>
            </div>
            <?php endif; ?>

        </div>

    </section>
