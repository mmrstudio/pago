
    <?php

        $form = get_sub_field('form');

    ?>

    <section class="section section--content-row content" data-hide-separator>

        <div class="section__container">

            <div class="section__inner">

                <div class="content__columns">

                        <div class="content__columns__column" data-width="full">

                            <div class="component component--form">
                                <?php echo $form ? do_shortcode('[gravityform id="'. $form['id'] .'" title="false" description="false"]') : ''; ?>
                            </div>

                        </div>

                </div>

            </div>

        </div>

    </section>
