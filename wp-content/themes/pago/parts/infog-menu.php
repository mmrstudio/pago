<?php

    $args = array(
        'post_type' => 'page',
        'posts_per_page'=> -1,
        'post_status' => 'publish',
        'post_parent' => $post_parent //Set at the template
    );

    $loop = new WP_Query($args);
    $result = array();
?>

<div class="infog-menu">
    <div class="infog-menu__totop">Change payment method type</div>
        <ul class="infog-menu__links">
<?php

    while ( $loop->have_posts() ) : $loop->the_post();

      $path = parse_url(get_the_permalink());
      $path = $path['path'];
?>

        <li><a href="<?php the_permalink() ?>" class="infog-menu__link"><?php the_title() ?></a></li>

<?php
    endwhile;
    wp_reset_query();
?>
    </ul>
</div>