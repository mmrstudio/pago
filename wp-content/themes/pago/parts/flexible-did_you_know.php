
    <section class="section section--did_you_know content" data-background="red">

        <div class="section__container">

            <div class="section__inner">

                <?php if(get_sub_field('section_title')) : ?>
                <div class="section__title">
                    <h2><?php echo get_sub_field('section_title'); ?></h2>
                </div>
                <?php endif; ?>

                <div class="content__columns">

                    <div class="section--did_you_know__image content__columns__column" data-width="third">

                        <div class="section--did_you_know__image__inner">
                            <?php echo acf_image(get_sub_field('image'), 'large', true); ?>
                        </div>

                    </div>

                    <div class="section--did_you_know__points content__columns__column" data-width="two_thirds">

                        <div class="section--did_you_know__points__inner">

                            <div class="content__columns">

                                <?php foreach(get_sub_field('points') as $point) : ?>
                                <div class="section--did_you_know__points__point content__columns__column" data-width="half">
                                    <h3 class="section--did_you_know__points__point__title"><?php echo $point['point_title']; ?></h3>
                                    <div class="section--did_you_know__points__point__description">
                                        <?php echo $point['description']; ?>
                                    </div>
                                </div>
                                <?php endforeach; ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
