<section  class="section">
    <div  class="section__container">
        <div class="section__title section__title--center">
            <h2 class="no-margin"><?php echo get_sub_field('heading'); ?></h2>
        </div>
    </div>
</section>
