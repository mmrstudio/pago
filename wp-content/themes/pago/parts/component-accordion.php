
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--accordion">

            <?php foreach($component['panels'] as $i => $panel) : ?>

            <div class="component--accordion__panel content__collapsible">
                <div class="component--accordion__panel__toggle content__collapsible__toggle"><?php echo $panel['panel_title']; ?></div>
                <div class="component--accordion__panel__content content__collapsible__content">
                    <div class="component--accordion__panel__content__inner">
                        <?php echo $panel['panel_content']; ?>
                    </div>
                </div>
            </div>

            <?php endforeach; ?>

        </div>

    </div>
