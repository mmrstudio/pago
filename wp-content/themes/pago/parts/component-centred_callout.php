
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--centred-callout">
            <h1 class="component--centred-callout__title"><?php echo $component['title']; ?></h1>
            <div class="component--centred-callout__text">
                <?php echo $component['text']; ?>
            </div>
            <?php acf_component_buttons($component); ?>
        </div>

    </div>
