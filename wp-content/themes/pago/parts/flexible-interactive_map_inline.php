
    <?php

        $content = [
            'china' => [
                'title' => get_sub_field('china_title'),
                'introduction' => get_sub_field('china_introduction'),
                'link' => get_sub_field('china_link'),
            ],
            'thailand' => [
                'title' => get_sub_field('thailand_title'),
                'introduction' => get_sub_field('thailand_introduction'),
                'link' => get_sub_field('thailand_link'),
            ],
            'indonesia' => [
                'title' => get_sub_field('indonesia_title'),
                'introduction' => get_sub_field('indonesia_introduction'),
                'link' => get_sub_field('indonesia_link'),
            ],
            'button_text' => get_sub_field('button_text'),
        ];

    ?>

    <section class="section section--interactive-map--inline" data-map-content='<?php echo json_encode($content); ?>'></section>
