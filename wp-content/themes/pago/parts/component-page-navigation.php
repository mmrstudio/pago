
    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="component component--page-navigation">

            <nav class="post_nav">
                <div class="nav-previous"><?php previous_post_link($component['previous_label'], '&larr; Previous'); ?></div>
                <div class="nav-next"><?php next_post_link($component['next_label'], 'Next &rarr;'); ?></div>
            </nav>

        </div>

    </div>
