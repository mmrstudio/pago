<!DOCTYPE HTML>

<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title><?php echo wp_title(); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="apple-touch-icon-precomposed" href="<?php echo THEME_URL; ?>/dist/images/favicon.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="<?php echo THEME_URL; ?>/dist/images/favicon.png">
        <link rel="icon" href="<?php echo THEME_URL; ?>/dist/images/favicon.png">
        
<?php wp_head(); ?>

        <?php echo google_analytics(get_field('google_analytics_code', 'option')); ?>

    </head>

<body <?php body_class(); ?>>
<?php //<div id="fullpage">?>