<?php //<div class="infog-tiles"> ?>

    <?php while ( have_rows('info_tiles') ) : the_row(); ?>
                
        <div class="infog-tile infog-tile--<?php the_sub_field('info_tile_shade'); ?>">
            <div class="infog-tile__inner">
                <img class="infog-tile__img" src="<?php the_sub_field('info_tile_icon'); ?>">
                
                <?php
                    while ( have_rows('info_tile') ) : the_row();

                        if( get_row_layout() == 'info_tile_h1' ):
                            $text = get_sub_field('info_tile_h1_text');
                            $class = 'infog-tile__h1';
                            $modifier = $class.'--'.get_sub_field('info_tile_h1_colour');
                        elseif( get_row_layout() == 'info_tile_h2' ): 
                            $text = get_sub_field('info_tile_h2_text');
                            $class = 'infog-tile__h2';
                            $modifier = $class.'--'.get_sub_field('info_tile_h2_colour');
                        elseif( get_row_layout() == 'info_tile_h3' ): 
                            $text = get_sub_field('info_tile_h3_text');
                            $class = 'infog-tile__h3';
                            $modifier = $class.'--'.get_sub_field('info_tile_h3_colour');
                        endif;
                ?>
                <p class="<?php echo $class.' '.$modifier; ?>"><?php echo $text; ?></p>
                <?php
                    endwhile;
                ?>

            </div>
        </div>
    <?php endwhile; ?>

<?php //</div> ?>