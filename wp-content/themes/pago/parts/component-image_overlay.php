
    <?php

        $field = $component['image_type'];
        $overlay_class = '';

        if($field == 'circle'){
            $overlay_class = 'omponent component--image-overlay-circle';
        }
        
    ?>

    <div class="content__columns__column" data-width="<?php echo $component['width']; ?>">

        <div class="<?php echo $overlay_class; ?>--wrapper">
            <div class="component component--image-overlay <?php echo $overlay_class; ?>">

                <a href="<?php echo link_url($component['link_type'], $component['link_internal'], $component['link_external']); ?>" class="component--image-overlay__overlay">
                    <div class="component--image-overlay__overlay__inner">

                        <?php if($field == 'circle'){ ?>
                            <p class="component--image-overlay__link">
                                <strong><?php echo $component['link_text']; ?></strong>
                            </p>
                        <?php }else{ ?>
                            <h4><?php echo $component['heading']; ?></h4>
                            <hr>
                            <?php echo $component['text']; ?>
                            <p class="component--image-overlay__link">
                                <strong><?php echo $component['link_text']; ?></strong>
                            </p>
                        <?php } ?>
                        
                    </div>
                </a>

                <div title="<?php echo $component['image']['title']; ?>" class="component--image-overlay__image" style="background-image: url(<?php echo acf_image($component['image'], false, false); ?>);">
                    <?php //echo acf_image($component['image'], false, true); ?>
                </div>

                

            </div>
        </div>

        <?php acf_component_content($component); ?>
            <?php acf_component_buttons($component); ?>

    </div>
