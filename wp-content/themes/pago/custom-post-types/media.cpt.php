<?php

    // define cpt
    $cpt_labels = array(
        'post_type_name' => 'media',
        'singular' => 'Media',
        'plural' => 'Media',
        'slug' => 'media'
    );

    // define cpt options
    $cpt_options = array(
        'supports' => array('title', 'editor', 'thumbnail'),
        'rewrite' => array(
            'slug' => 'media',
            'pages' => false,
            'with_front' => true,
        ),
        'has_archive' => true,
        'menu_position' => 20,
    );

    // create cpt
    $media_cpt = new CPT($cpt_labels, $cpt_options);

    // set dashicon
    $media_cpt->menu_icon('dashicons-images-alt');

    // taxonomies

    $media_cpt->register_taxonomy(array(
    	'taxonomy_name' => 'media_territory',
    	'singular' => 'Territory',
    	'plural' => 'Territories',
    	'slug' => 'media_territory'
    ));

    $media_cpt->register_taxonomy(array(
    	'taxonomy_name' => 'media_solutions',
    	'singular' => 'Solution',
    	'plural' => 'Solutions',
    	'slug' => 'media_solutions'
    ));

    $media_cpt->register_taxonomy(array(
    	'taxonomy_name' => 'media_category',
    	'singular' => 'Category',
    	'plural' => 'Categories',
    	'slug' => 'media_category'
    ));

    $media_cpt->register_taxonomy(array(
    	'taxonomy_name' => 'media_medium',
    	'singular' => 'Medium',
    	'plural' => 'Mediums',
    	'slug' => 'media_medium'
    ));
