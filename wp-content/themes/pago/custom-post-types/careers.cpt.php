<?php

    // define cpt
    $cpt_labels = array(
        'post_type_name' => 'position',
        'singular' => 'Position',
        'plural' => 'Careers',
        'slug' => 'careers'
    );

    // define cpt options
    $cpt_options = array(
        'supports' => array('title',  'excerpt', 'editor'),
        'rewrite' => array(
            'slug' => 'position',
            'pages' => false,
            'with_front' => true,
        ),
        'has_archive' => false,
        'menu_position' => 20,
    );

    // create cpt
    $careers_cpt = new CPT($cpt_labels, $cpt_options);

    // set dashicon
    $careers_cpt->menu_icon('dashicons-clipboard');

    add_filter( 'gform_pre_render_2', 'populate_posts' );
    add_filter( 'gform_pre_validation_2', 'populate_posts' );
    add_filter( 'gform_pre_submission_filter_2', 'populate_posts' );
    add_filter( 'gform_admin_pre_render_2', 'populate_posts' );

    function populate_posts( $form ) {

        foreach ( $form['fields'] as &$field ) {

            if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-positions' ) === false ) {
                continue;
            }

            $posts = get_posts( 'post_type=position&numberposts=-1&post_status=publish' );

            $choices = array();

            foreach ( $posts as $post ) {
                $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
            }

            // update 'Select a Post' to whatever you'd like the instructive option to be
            $field->placeholder = 'Select a position...';
            $field->choices = $choices;

        }

        return $form;
    }
